@servers(['web' => 'deploy@freevoicelessons.jeffharris.us'])

@setup
    $repository = 'git@gitlab.com:jefhar/freevoicelessons.git';
    $release = date('YmdHis');
    $composer_install = '/home/deploy/bin/composer install --no-dev -q -o';
    $app_dir = isset($app_dir) ? $app_dir: '/var/www/net/freevoicelessons';
    $release_url = isset($release_url) ? $release_url : 'https://www.freevoicelessons.net';
    $releases_dir = $app_dir . '/releases';
    $new_release_dir = $releases_dir . '/' . $release;
@endsetup

@story('test')
    testecho
@endstory

@task('testecho')
    echo "Deploying to {{ $release_url }}"
    echo "Using directory {{ $app_dir }}"
@endtask

@story('deploy')
    clone_repository
    run_composer
    update_storage
    update_cache
    update_env
    migrate
    update_docroot
    optimize
@endstory

@task('clone_repository')
    [ -d {{ $releases_dir }} ] || mkdir -p {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment #{{ $release }}"
    cd {{ $new_release_dir }}
    {{ $composer_install }}
@endtask

@task('optimize')
    cd {{ $new_release_dir }}
    php artisan config:clear
    php artisan config:cache
    {{-- php artisan route:cache --}}
@endtask

@task('update_storage')
    echo "Linking storage directory."
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage
    echo "Checking permissions on storage"
    sudo chgrp -R www-data {{ $new_release_dir }}/storage
@endtask

@task('update_cache')
    sudo chgrp -R www-data {{ $new_release_dir }}/bootstrap/cache
@endtask

@task('update_env')
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
@endtask

@task('update_docroot')
    echo 'Linking webroot to current release.'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('passport_install')
    if [ ! -f {{ $new_release_dir }}/storage/oauth-public.key ]; then
    echo 'Installing Passport.'
    cd {{ $new_release_dir }}
    php artisan passport:install
    else
    echo 'Passport already installed.'
    fi
@endtask

@task('migrate')
    echo 'Migrating database'
    cd {{ $new_release_dir }}
    php artisan migrate --force
@endtask

@finished
    @slack($web_hook, '#fvl-logging', ':robot_face: New deployment pushed to ' . $release_url)
@endfinished
