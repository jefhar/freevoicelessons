@extends('layouts.public')

@section('article')
    <article id="index" class="mt-10 mb-1 mx-4 leading-loose">
        <header class="entry-header">
            <h1 class="entry-title">The Program</h1>
        </header>

        <figure id="attachment_8" style="width: 300px" class="pl-3 float-right">
            <img class="border-black border"
                 src="/images/SeatedClass.jpg"
                 alt="Session 1 of Voice Clinic with instructor, Paul Eastman." width="300" height="125">
            <figcaption class="text-sm leading-normal -mt-2">Session 1 of Voice Clinic with instructor, Paul
                Eastman.
            </figcaption>
        </figure>
        <p>The Singer&#8217;s Voice Clinic is a free seven-week vocal clinic for men, women, and children
            (accompanied by an adult). The objective is to provide you the fundamentals of</p>
        <ol>
            <li>Voice Care</li>
            <li>Quality Sound Production</li>
            <li>Ensemble vs Solo Singing</li>
        </ol>
        @if (is_null($nextCourse))
            <p>The next course has not been scheduled. Please come back in a bit.
        @else
            <p>The next program will begin on {{ date('l, F j, Y', strtotime($nextCourse->start_date)) }}
                and run through {{ date('l, F j, Y', strtotime($nextCourse->end_date)) }}.
                @endif
                Clinic sessions will begin promptly at 7:30 PM with the Peninsulaires. Each lesson in the
                series will run approximately 90 to 120 minutes. Students will gain the most by attending
                all
                @if($nextCourse)
                    {{ $nextCourse->meetings->count() }}
                @endif
                sessions. Nevertheless, each topic is a stand-alone subject and you will benefit from
                attending as many lessons as possible.</p>
            <p>This program is sponsored by <a
                        href="https://www.barbershop-harmony.org/?utm_source=freevoicelessons&amp;utm_medium=link&amp;utm_content=1stPara&amp;utm_campaign=Peninsulaires"
                        target="_blank" rel="noopener noreferrer">The Peninsulaires Men&#8217;s Chorus</a>,
                The Palo Alto-Mountain View Chapter of the Barbershop Harmony Society. The clinic will be
                conducted at the Peninsulaires&#8217; rehearsal hall in the Sunnyvale Elks Lodge, 375 N.
                Pastoria Ave, Calif. The
                @if ($nextCourse)
                    week {{ $nextCourse->meetings->count() }}
                @else
                    last
                @endif
                session will be an Open House and graduation ceremony.</p>
            <h2>The Weekly schedule</h2>
            <figure id="attachment_8" style="width: 300px" class="pl-3 float-right">
                <img class="border-black border"
                     src="https://www.freevoicelessons.net/wp-content/uploads/2016/03/Risers-300x149.jpg"
                     alt="Clinic students on the risers after post clinic performance of It's a Grand Old Flag "
                     width="300" height="149"/>
                <figcaption class="text-sm leading-normal -mt-2">Clinic students on the risers after post
                    clinic performance of It&#8217;s a Grand Old Flag
                </figcaption>
            </figure>
            <p>The Singer&#8217;s Voice Clinic will address topics such as: How to properly “warm up” the
                voice; the importance of posture; the use of the body&#8217;s resonance chambers; preventing
                getting hoarse; the impact of pronunciation on audience enjoyment, and much more.</p>
            <p>We are certain that, from the very first lesson, you will learn things you can immediately
                apply to your current singing activity. Whether you sing in the car, the karaoke lounge,
                with your church, a local ensemble, or a community musical you will find something in this
                program that will
                help make you a better vocalist.</p>
            @if($nextCourse)
                <h2>Here is the weekly schedule:</h2>
                <ul class="list-reset">
                    @foreach($nextCourse->meetings as $meeting)
                        <li>{{$meeting->date}} {{ $meeting->title }}</li>
                    @endforeach
                </ul>
            @endif
            <p>Remember, that even if you miss one week, each topic is a stand-alone subject and you may
                still attend the rest of the classes.</p>
            <p id="rsvp">RSVP and save your seat today!</p>
    </article>
@endsection
