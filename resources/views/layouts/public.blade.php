<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <link rel='stylesheet' id='tt-easy-google-fonts-css'
          href='https://fonts.googleapis.com/css?family=Open+Sans%3Aregular%7CMerriweather%3A700&#038;subset=latin%2Call&#038;ver=4.7.4'
          type='text/css' media='all'/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}!</title>
</head>
<body class="bg-orange-lightest font-sans font-normal antialiased">
<div class="flex flex-col">
    <img src="/images/SingersVoiceClinicBanner.png" alt="The Singer's Voice Clinic.">
    <div class="flex flex-row items-start">
        <div class="w-3/4">

            @yield('article')
        </div>
        <div class="w-1/4">
            <aside id="post-7" class="mt-12 mb-1 mx-4 leading-loose border-black border-t-2">
                <h3>PAGES</h3>
                <ul class="list-reset">
                    <li><a href="/">The Program</a></li>
                    <li><a href="/your-instructors">Your Instructors</a></li>
                    <li><a href="/testimonials">Testimonials</a></li>
                </ul>
            </aside>
        </div>
    </div>
    Parchment image copyright www.myfreetextures.com under a creative commons attribution licence.
</div>
</body>
</html>
