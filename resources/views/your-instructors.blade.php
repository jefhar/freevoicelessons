@extends('layouts.public')

@section('article')
    <article id="your-instructors" class="mt-10 mb-1 mx-4 leading-loose">
        <header class="entry-header">
            <h1 class="entry-title">Your Instructors</h1>
        </header>
        <h2>Dr. Sean Abel</h2>
        <figure id="dr_abel" style="width: 175px" class="pl-3 float-right">
            <img class="border-black border"
                 src="/images/DrAbel.jpg"
                 alt="Peninsulaires Director Sean Abel" width="175" height="237"/>
            <figcaption class="text-sm leading-normal -mt-2">Peninsulaires Director Dr. Sean Abel
            </figcaption>
        </figure>
        <p>Sean began barbershopping in February of 1989 in St. Joseph,
            Missouri with the Pony Expressmen Barbershop Chorus. Singing in the bass section he soon joined
            the leadership team as an assistant director. Later, he was assistant director of the Omaha,
            Nebraska Heartland Harmonizers and then the front line director of the Sioux City, Iowa
            Siouxlanders Chorus. Following a 7 year hiatus, he helped form the Jefferson City, Missouri
            Chapter and the Show Me Showboaters Chorus. After teaching music (mostly band) in the public
            schools of Maysville, Missouri, Emerson, Iowa, Moville, Iowa, and Jefferson City, Missouri, Sean
            moved to Weed, California to teach band, music history, vocal jazz, music theory and barbershop
            chorus at the College of the Siskiyous. Presently, Sean is the Division Dean at Evergreen Valley
            College. Sean holds a Bachelors of Music Education from the University of Kansas, a Masters of
            Education from Lindenwood University, and is a Doctoral Candidate in Music Education at Boston
            University.</p>
        <h2>Paul Eastman</h2>
        <figure id="paul_eastman" style="width: 235px" class="pl-3 float-right">
            <img class="border-black border"
                 src="/images/Paul-Eastman.jpg"
                 alt="Clinic instructor and Peninsulaires Assistant Director Paul Eastman" width="235"
                 height="177"/>
            <figcaption class="text-sm leading-normal -mt-2">Clinic instructor and Peninsulaires Assistant
                Director Paul Eastman
            </figcaption>
        </figure>
        <p>Paul has been singing, conducting, coaching,arranging and teaching
            barbershop music for more than forty years. More than 75 choruses and quartets have benefited
            from his coaching, ranging from the beginner level to International competitor. His choral
            experience includes nine appearances in the Barbershop Harmony Society&#8217;s International
            Chorus Competition, finishing as high as seventh place in the world. He has performed in many
            unusual venues, such as the Superbowl, the San Francisco Opera House, Gripsholm Castle in
            Sweden, the Waikiki Bowl, a tunnel inside Hoover Dam and Candlestick Park, where he sang the
            National Anthem for the SF Giants.</p>
        <h2>Your Hosts</h2>
        <figure id="peninsulaires" style="width: 300px" class="pl-3 float-right">
            <img class="border-black border"
                 src="/images/Peninsulaires-2017Contest.jpg"
                 alt="The Peninsulaires Barbershop Harmony Chorus" width="300" height="146"/>
            <figcaption class="text-sm leading-normal -mt-2">
                The Peninsulaires Barbershop Harmony Chorus
            </figcaption>
        </figure>
        <p>The Peninsulaires Men&#8217;s Chorus is part of the Palo Alto-Mountain View Chapter of The
            Barbershop Harmony Society, a non-profit music education organization. They are one of the first
            barbershop choruses established in the San Francisco Bay Area. For over 70 years they have been
            delighting audiences with the quality of their music, the power of their performances and the
            passion for their craft.</p>
        <p>The Peninsulaires have long been regarded as one of the premier performing choruses among the
            800+ chapters of the Barbershop Harmony Society. They are the reigning Chorus Champion in their
            Division at the &#8220;AA&#8221; level.</p>
        <p>As a non-profit music education organization, The Peninsulaires are committed to musical and
            performance excellence. They offer their members (and youth music groups in the
            community)instruction to help build the many skills of quality singing and musicality: sound
            production, resonance, breath support and music theory, as well as how to arrange music and/or
            direct a chorus.</p>
        <p>For more information visit their website at <a
                    href="https://www.barbershop-harmony.org/?utm_source=freevoicelessons&amp;utm_medium=weblink&amp;utm_campaign=VocalClinic"
                    target="_blank" rel="noopener noreferrer">www.Barbershop-Harmony.org</a>.<br/>
        </p>
    </article>
@endsection
