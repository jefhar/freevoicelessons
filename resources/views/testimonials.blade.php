@extends('layouts.public')

@section('article')
    <article id="testimonials" class="mt-10 mb-1 mx-4 leading-loose">
        <header class="entry-header">
            <h1 class="entry-title">Testimonials</h1>
        </header>

        <figure id="graduation" style="width: 200px" class="pl-3 float-right">
            <img class="border-black border"
                 src="/images/graduation.jpg"
                 alt="Clinic students receiving their Certificate of Completion and acknowledgement from members of The Peninsulaires chorus."
                 width="200" height="173"/>
            <figcaption class="text-sm leading-normal -mt-2">
                Clinic students receiving their Certificate of Completion and acknowledgement from members of The
                Peninsulaires chorus.
            </figcaption>
        </figure>
        <section>
            <p style="text-align: left;"><i>Thank you all tremendously for the generous gift you gave to me and the many
                    other men.</i></p>
            <p><i>Although I sang continuously from 4th grade through college, was part of a special choral group in
                    high school that toured Romania, and even sang Barbershop for three years as first-tenor in The
                    Logarhythms in college, I have never in my 54 years had, until now, any training in singing. It was
                    astonishing to realize, and I’m very very very happy with what I’ve now been taught.</i><br/>
                <span style="text-align: right; display: block;">—Dave M.</span></p>
        </section>

        <section>
            <p style="text-align: left;"><i>Paul Eastman is the kind of teacher everyone wants to be. Breaking things
                    down into easy pieces and reviewing each week really helped.</i><br/>
                <span style="text-align: right; display: block;">—Brian D.</span></p>
        </section>

        <section>
            <p style="text-align: left;"><i>The individual work really gave an appreciation for how different everyone’s
                    voice could be and how to change things to sound better.</i><br/>
                <span style="text-align: right; display: block;">—Alex J.</span></p>
        </section>

        <h2 style="text-align: left;">How To Register</h2>
        <figure id="students" style="width: 200px" class="pl-3 float-right">
            <img class="border-black border"
                 src="/images/TightClass.jpg"
                 alt="TightClass" width="300" height="140"/>
        </figure>
        <p style="text-align: left;">
            The Singer&#8217;s Voice Clinic is a free seven week program sponsored by The Peninsulaires Men&#8217;s
            Chorus.</p>
        <p>To RSVP and save your seat, complete the form on the main page. You will receive e-mail confirmation of
            your registration along with specific directions to the venue location.</p>
        <p>For more information contact Jerry at thepeninsulaires@gmail.com.

        </p>
    </article>
@endsection
