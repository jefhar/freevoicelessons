<?php

namespace Tests\Feature;

use App\Testimonial;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CanCreateTestimonialsTest extends TestCase
{
    use WithFaker;
    /**
     * @test
     */
    public function aStudentCanHaveTestimonials()
    {
        $user = factory(User::class)->create();
        $testimonial = factory(Testimonial::class)->make();
        $user->testimonials()->save($testimonial);
        $user->save();

        $this->assertDatabaseHas(Testimonial::TABLE, [Testimonial::USER_ID => $user->id]);
    }

    /**
     * @test
     */
    public function aTestimonialHasABody()
    {
        $testimonial = factory(Testimonial::class)->create();
        $this->assertDatabaseHas(Testimonial::TABLE, [Testimonial::BODY => $testimonial->body]);
    }

    /**
     * @test
     */
    public function aTestimonialHasAStudentName()
    {
        $firstName = $this->faker->firstName();
        $lastName = $this->faker->lastName();
        $nameShouldBe = $firstName . ' ' . substr($lastName, 0, 1);
        $user = factory(User::class)->create([
            User::NAME => $firstName . ' ' . $lastName,
        ]);
        $testimonial = factory(Testimonial::class)->make();
        $user->testimonials()->save($testimonial);

        $dbTestimonial = Testimonial::first();
        $this->assertEquals($nameShouldBe, $dbTestimonial->user->testimonialName);
    }
}
