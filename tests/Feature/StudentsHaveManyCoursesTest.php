<?php

namespace Tests\Feature;

use App\Course;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StudentsHaveManyCoursesTest extends TestCase
{

    /**
     * @test
     */
    public function studentsHaveManyCourses()
    {
        $alice = factory(User::class)->create([User::NAME => 'Alice']);
        $bob = factory(User::class)->create([User::NAME => 'Bob']);
        $charlie = factory(User::class)->create([User::NAME => 'Charlie']);

        $aerobics = factory(Course::class)->create([Course::TITLE => 'Aerobics']);
        $band = factory(Course::class)->create([Course::TITLE => 'Band']);
        $choir = factory(Course::class)->create([Course::TITLE => 'Choir']);

        $alice->courses()->attach($aerobics);
        $alice->courses()->attach($choir);
        $bob->courses()->attach($band);
        $bob->courses()->attach($choir);
        $charlie->courses()->attach($choir);

        $this->assertDatabaseHas('course_user', [User::ID => $alice->id, Course::ID => $aerobics->id]);
        $this->assertDatabaseHas('course_user', [User::ID => $alice->id, Course::ID => $choir->id]);
        $this->assertDatabaseHas('course_user', [User::ID => $bob->id, Course::ID => $band->id]);
        $this->assertDatabaseHas('course_user', [User::ID => $bob->id, Course::ID => $choir->id]);
        $this->assertDatabaseHas('course_user', [User::ID => $charlie->id, Course::ID => $choir->id]);

        $course = $charlie->courses()->first();
        $this->assertEquals($choir->title, $course->title);
    }
}
