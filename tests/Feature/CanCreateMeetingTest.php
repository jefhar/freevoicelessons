<?php

namespace Tests\Feature;

use App\Meeting;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CanCreateMeetingTest extends TestCase
{
    /**
     * @test
     */
    public function meetingHasRequiredTitleField()
    {
        $meeting = factory(Meeting::class)->make();
        $meeting->save();
        $this->assertDatabaseHas(Meeting::TABLE, [Meeting::TITLE => $meeting->title]);

        $newMeeting = new Meeting();
        $newMeeting->save();
        $this->assertDatabaseHas(Meeting::TABLE, [Meeting::TITLE => $newMeeting->title]);
    }

    /**
     * @test
     */
    public function meetingHasDateField()
    {
        $meeting = factory(Meeting::class)->create();
        $this->assertDatabaseHas(Meeting::TABLE, [Meeting::DATE => $meeting->date]);
    }

    /**
     * @test
     */
    public function meetingHasStartTime()
    {
        $meeting = factory(Meeting::class)->create();
        $this->assertDatabaseHas(Meeting::TABLE, [Meeting::START_TIME => $meeting->start_time]);
    }

    /**
     * @test
     */
    public function meetingHasEndTime()
    {
        $meeting = factory(Meeting::class)->create();
        $this->assertDatabaseHas(Meeting::TABLE, [Meeting::END_TIME => $meeting->end_time]);
    }

    /**
     * @test
     */
    public function meetingHasBody()
    {
        $meeting = factory(Meeting::class)->create();
        $this->assertDatabaseHas(Meeting::TABLE, [Meeting::BODY => $meeting->body]);
    }

    /**
     * @test
     */
    public function meetingHasAttachment()
    {
        Storage::fake('syllabus');
        $attachment = UploadedFile::fake()->create('attachment.pdf', 30);
        $meeting = factory(Meeting::class)->create();
        $meeting->attachment = $attachment->getPathname();

        $meeting->save();
        $this->assertDatabaseHas(Meeting::TABLE, [Meeting::ATTACHMENT => $meeting->attachment]);
    }
}
