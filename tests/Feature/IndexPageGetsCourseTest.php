<?php

namespace Tests\Feature;

use App\Course;
use App\Meeting;
use Tests\TestCase;

class IndexPageGetsCourseTest extends TestCase
{
    /**
     * @test
     */
    public function indexPagePrintsWithNoUpcomingCourses()
    {
        $response = $this->get('/');
        $response->assertOK();
        $response->assertSeeInOrder(['The', 'last', 'session']);
    }

    /**
     * @test
     */
    public function indexPagePrintsWithUpcomingCourse()
    {
        $course = factory(Course::class)->create([
            Course::IS_ACTIVE => true,
            Course::START_DATE => date('Y-m-d'),
            Course::END_DATE => date('Y-m-d', strtotime('+6 weeks')),
        ]);
        $weeks = [
            factory(Meeting::class)->create([
                Meeting::DATE => date('Y-m-d'),
            ]),
            factory(Meeting::class)->create([
                Meeting::DATE => date('Y-m-d', strtotime('+1 week')),
            ]),
            factory(Meeting::class)->create([
                Meeting::DATE => date('Y-m-d', strtotime('+2 week')),
            ]),
            factory(Meeting::class)->create([
                Meeting::DATE => date('Y-m-d', strtotime('+3 week')),
            ]),
            factory(Meeting::class)->create([
                Meeting::DATE => date('Y-m-d', strtotime('+4 week')),
            ]),
            factory(Meeting::class)->create([
                Meeting::DATE => date('Y-m-d', strtotime('+5 week')),
            ]),
            factory(Meeting::class)->create([
                Meeting::DATE => date('Y-m-d', strtotime('+6 week')),
            ]),
        ];
        $course->meetings()->saveMany($weeks);
        $course->save();

        $response = $this->get('/');
        $response->assertSee("<h2>Here is the weekly schedule:</h2>");
        $response->assertSee('The next program will begin on ' . date('l, F d, Y', strtotime($course->start_date)));
        $response->assertSee('and run through ' . date('l, F j, Y', strtotime($course->end_date)));
        foreach ($weeks as $week) {
            $response->assertSee(date('Y-m-d', strtotime($week->date)) . ' ' . $week->title);
        }
    }
}
