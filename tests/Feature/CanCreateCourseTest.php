<?php

namespace Tests\Feature;

use App\Course;
use App\Meeting;
use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CanCreateCourseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * @test
     */
    public function courseHasATitleField()
    {
        $course = factory(Course::class)->create();
        $this->assertDatabaseHas(Course::TABLE, [Course::TITLE => $course->title]);

        $newCourse = new Course();
        $newCourse->save();
        $this->assertDatabaseHas(Course::TABLE, [Course::TITLE => $course->title]);
    }

    /**
     * @test
     */
    public function courseHasAnIsActiveField()
    {
        $course = factory(Course::class)->create();
        $this->assertDatabaseHas(Course::TABLE, [Course::IS_ACTIVE => $course->is_active]);
    }

    /**
     * @test
     */
    public function courseHasAStartDateField()
    {
        $course = factory(Course::class)->create();
        $this->assertDatabaseHas(Course::TABLE, [Course::START_DATE => $course->start_date]);
    }

    /**
     * @test
     */
    public function courseHasAnEndDateField()
    {
        $course = factory(Course::class)->create();
        $this->assertDatabaseHas(Course::TABLE, [Course::END_DATE => $course->end_date]);
    }

    /**
     * @test
     */
    public function courseHasASyllabusFileField()
    {
        Storage::fake('syllabus');
        $syllabus = UploadedFile::fake()->create('syllabus.pdf', 30);

        $course = factory(Course::class)->make();
        $course->syllabus = $syllabus->getPathname();
        $course->save();

        $this->assertDatabaseHas(Course::TABLE, [Course::SYLLABUS => $course->syllabus]);
    }

    /**
     * @test
     */
    public function courseHasMeetings()
    {
        $course = factory(Course::class)->create();

        $meetings = factory(Meeting::class, 3)->make();
        $course->meetings()->saveMany($meetings);
        $this->assertDatabaseHas(Meeting::TABLE, [Meeting::COURSE_ID => $course->id]);
        $this->assertDatabaseHas(Course::TABLE, [Course::ID => $course->id]);
    }

    /**
     * @test
     */
    public function courseHasStudents()
    {
        $course = factory(Course::class)->create();

        $users = factory(User::class, 3)->make();
        $course->students()->saveMany($users);
        $this->assertDatabaseHas('course_user', [
            [
                'user_id' => "{$users[0]->id}",
                'course_id' => "{$course->id}",
            ],
        ]);
    }
}
