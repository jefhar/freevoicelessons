<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    const COURSE_ID = 'course_id';
    const DATE = 'date';
    const DEFAULT_START_TIME = '09:00:00';
    const DEFAULT_TITLE = 'Meeting Title';
    const END_TIME = 'end_time';
    const ID = 'id';
    const START_TIME = 'start_time';
    const TABLE = 'meetings';
    const TITLE = 'title';
    const DEFAULT_END_TIME = '17:00:00';
    const BODY = 'body';
    const ATTACHMENT = 'attachment';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        self::TITLE => self::DEFAULT_TITLE,
        self::START_TIME => self::DEFAULT_START_TIME,
        self::END_TIME => self::DEFAULT_END_TIME,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
