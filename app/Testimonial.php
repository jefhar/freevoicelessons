<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    const TABLE = 'testimonials';
    const ID = 'id';
    const USER_ID = 'user_id';
    const BODY = 'body';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
