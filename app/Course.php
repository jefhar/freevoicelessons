<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    const END_DATE = 'end_date';
    const ID = 'id';
    const IS_ACTIVE = 'is_active';
    const START_DATE = 'start_date';
    const SYLLABUS = 'syllabus';
    const TABLE = 'courses';
    const TITLE = 'title';
    const DEFAULT_TITLE = 'Course Title';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        self::IS_ACTIVE => false,
        self::TITLE => self::DEFAULT_TITLE,
    ];

    /**
     * Attaching a student updates the User's updated_at.
     *
     * @var array
     */
    protected $touches = [User::TABLE];

    public function users()
    {
        return $this->students();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function students()
    {
        return $this->belongsToMany(User::class)
            ->as('students')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meetings()
    {
        return $this->hasMany(Meeting::class);
    }
}
