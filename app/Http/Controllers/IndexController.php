<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $nextStartingCourse = Course::where([
            [Course::IS_ACTIVE, true],
            [Course::START_DATE, '>=', date('Y-m-d')],
        ])->orderBy(Course::START_DATE)->with('meetings')->first();

        return view('index', ['nextCourse' => $nextStartingCourse]);
    }
}
