<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, Notifiable;

    const EMAIL = 'email';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const NAME = 'name';
    const PASSWORD = 'password';
    const TABLE = 'users';
    const TIMEZONE = 'timezone';
    const ID = 'id';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        self::TIMEZONE => 'America/Los_Angeles',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'name',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getTestimonialNameAttribute()
    {
        $nameParts = explode(' ', $this->name);
        return $nameParts[0] . ' ' . substr($nameParts[array_key_last($nameParts)], 0, 1);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class)->withTimestamps();
    }

    public function testimonials()
    {
        return $this->hasMany(Testimonial::class);
    }
}
