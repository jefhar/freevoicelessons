<?php
/**
 * Copyright 2018 Jeff Harris
 * PHP Version 7.3
 */
declare(strict_types=1);

namespace App\Permissions;

class UserPermissions
{
    const CREATE_COURSE = 'create course';
    const CREATE_LEADERS = 'create leaders';
    const CREATE_MEETINGS = 'create meetings';
    const EDIT_COURSES = 'edit courses';
    const EDIT_MEETINGS = 'edit meetings';
    const REGISTER_FOR_COURSE = 'register for course';
    const SEND_EMAIL = 'send email';
}
