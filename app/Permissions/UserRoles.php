<?php
/**
 * Copyright 2018 Jeff Harris
 * PHP Version 7.3
 */
declare(strict_types=1);

namespace App\Permissions;

class UserRoles
{
    const ADMIN = 'Admin';
    const GUEST = 'Guest';
    const LEADER = 'Leader';
    const STUDENT = 'Student';
}
