[![pipeline status](https://gitlab.com/jefhar/freevoicelessons/badges/master/pipeline.svg)](https://gitlab.com/jefhar/freevoicelessons/commits/master)
[![coverage report](https://gitlab.com/jefhar/freevoicelessons/badges/master/coverage.svg)](https://jefhar.gitlab.io/freevoicelessons)

This is the code for a site to collect registrations for Free Voice Lessons.
It contains static pages, a student registration page, an administrative
login page, and back end.

Through the back end, Admin can:
- Create a new course of Free Voice Lessons.
- Send bulk email to students.
- Update course and session information.
- Push advertising/notices to social media.

After defining the beginning and ending dates of each course, sessions must be
added to the course. Information can be added to each session for the students,
and the course must be made active.

Students register for an active course.

## Demo
Please see the demo at https://<<coming soon!>>. Login as an admin using email
`demo@example.com` and password `demopassword`. Please note that the demo site's
database completely resets every hour and, because it's a demo site, it does not
email nor connect to social media.

## Contributing

Fork, commit, [merge request](https://gitlab.com/jefhar/freevoicelessons/merge_requests).

## License

Open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## `Make`ing
- `make` will create and run the docker containers then install composer requirements
then run any migrations.
- `make docker` will create and run the docker containers.
- `make down` will close the docker containers.
- `make install` will install the composer package requirements.
- `make update` will update the composer package requirements.
- `make clean` will prune the docker system.
- `make migrate` will install the database migrations.
