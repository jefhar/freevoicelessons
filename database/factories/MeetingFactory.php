<?php

use App\Meeting;
use Faker\Generator as Faker;

$factory->define(App\Meeting::class, function (Faker $faker) {
    return [
        Meeting::BODY => $faker->text(200),
    ];
});
