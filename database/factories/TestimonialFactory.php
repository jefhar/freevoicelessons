<?php

use App\Testimonial;
use Faker\Generator as Faker;

$factory->define(Testimonial::class, function (Faker $faker) {
    return [
        Testimonial::BODY => $faker->text(200),
    ];
});
