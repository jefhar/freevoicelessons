<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => env('SEEDER_USER_NAME', 'Demo User'),
            'email' => env('SEEDER_USER_EMAIL', 'demo@example.com'),
            'password' => Hash::make((env('SEEDER_USER_PASSWORD', 'demopassword'))),
        ]);
    }
}
