<?php

use App\Course;
use App\Meeting;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createMeetingsAndCourse(strtotime("+10 days"));
        $this->createMeetingsAndCourse(strtotime("+4 months"));
    }

    protected function createMeetingsAndCourse(int $startDate)
    {
        $meetings = $this->makeWeeklyMeetings($startDate);
        $course = $this->makeCourseFromMeetings($meetings);
        $course->meetings()->saveMany($meetings);
    }

    /**
     * @param int $startDate
     * @return \Illuminate\Support\Collection
     */
    protected function makeWeeklyMeetings(int $startDate): \Illuminate\Support\Collection
    {
        $faker = Faker::create();
        $meetings = collect();
        $meeting = Meeting::create(
            [
                Meeting::BODY => $faker->text(200),
                Meeting::DATE => date('Y-m-d', $startDate),
                Meeting::END_TIME => '22:00:00',
                Meeting::START_TIME => '19:30:00',
                Meeting::TITLE => $faker->text(50),
            ]
        );
        $meetings->push($meeting);

        for ($i = 1; $i < rand(4, 10); $i++) {
            $meeting = Meeting::create(
                [
                    Meeting::BODY => $faker->text(200),
                    Meeting::DATE => date('Y-m-d', strtotime("+7 days", strtotime($meetings->last()->date))),
                    Meeting::END_TIME => '22:00:00',
                    Meeting::START_TIME => '19:30:00',
                    Meeting::TITLE => $faker->text(50),
                ]
            );
            $meetings->push($meeting);
        }

        return $meetings;
    }

    /**
     * @param \Illuminate\Support\Collection $meetings
     * @return mixed
     */
    protected function makeCourseFromMeetings(\Illuminate\Support\Collection $meetings)
    {
        $course = Course::create(
            [
                Course::END_DATE => $meetings->last()->date,
                Course::IS_ACTIVE => true,
                Course::START_DATE => $meetings->first()->date,
                Course::TITLE => 'Course starting ' . date('l, F jS, Y', strtotime($meetings->first()->date)),
            ]
        );

        return $course;
    }
}
