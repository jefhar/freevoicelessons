<?php

use App\Meeting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Meeting::TABLE, function (Blueprint $table) {
            $table->increments(Meeting::ID);
            $table->unsignedInteger(Meeting::COURSE_ID)->nullable();
            $table->char(Meeting::TITLE, 128)->default(Meeting::DEFAULT_TITLE);
            $table->date(Meeting::DATE)->nullable();
            $table->time(Meeting::START_TIME)->default(Meeting::DEFAULT_START_TIME);
            $table->time(Meeting::END_TIME)->default(Meeting::DEFAULT_END_TIME);
            $table->string(Meeting::BODY)->nullable();
            $table->char(Meeting::ATTACHMENT)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
