<?php

use App\Course;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Course::TABLE, function (Blueprint $table) {
            $table->increments(Course::ID);
            $table->char(Course::TITLE, 64)->default(Course::DEFAULT_TITLE);
            $table->boolean(Course::IS_ACTIVE)->default(false);
            $table->date(Course::START_DATE)->nullable();
            $table->date(Course::END_DATE)->nullable();
            $table->string(Course::SYLLABUS, 260)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
