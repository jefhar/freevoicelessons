<?php

use App\Testimonial;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Testimonial::TABLE, function (Blueprint $table) {
            $table->increments(Testimonial::ID);
            $table->unsignedInteger(Testimonial::USER_ID)->nullable();
            $table->string(Testimonial::BODY);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
    }
}
