migrate: install
	docker-compose exec php-fpm php artisan migrate

seed:
	docker-compose exec php-fpm composer dump-autoload
	docker-compose exec php-fpm php artisan db:seed

seed-dummy:
	docker-compose exec php-fpm composer dump-autoload
	docker-compose exec php-fpm php artisan migrate:fresh --seed
	docker-compose exec php-fpm php artisan db:seed --class=UsersTableSeeder

refresh:
	docker-compose exec php-fpm php artisan migrate:refresh --seed

docker: build
	docker-compose build
	docker-compose up -d

down:
	docker-compose down

install: docker
	docker-compose exec php-fpm composer install --no-plugins --no-scripts
	docker-compose exec php-fpm php -r "file_exists('.env') || copy ('.env.example', '.env');"
	docker-compose exec php-fpm php artisan key:generate

update: docker
	docker-compose exec php-fpm compser update --no-plugins --no-scripts

clean:
	docker system prune

test:
	docker-compose exec php-fpm vendor/bin/phpunit

build:
	docker build -t registry.gitlab.com/jefhar/freevoicelessons:latest .

ci:
	docker login registry.gitlab.com
	gitlab-runner exec docker test
