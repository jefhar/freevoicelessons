# registry.gitlab.com/jefhar/freevoicelessons:latest
# For unit testing and deployment
# Set the base image for subsequent instructions
FROM phpdockerio/php73-fpm:latest

# Update packages
RUN apt-get update \
    && apt-get -y --no-install-recommends install \
        php7.3-bcmath \
        php7.3-mysql \
        php7.3-phpdbg \
        php7.3-sqlite3 \
        php-redis \
    #   php-xdebug \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process

# Install Laravel Envoy
RUN composer self-update \
	&& composer global require "laravel/envoy=~1.0" \
	&& composer clear-cache
